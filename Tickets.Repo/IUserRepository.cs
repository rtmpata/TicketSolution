﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;
using System.Data.Entity;

namespace Tickets.Repo
{
    public interface IUserRepository : IGenericRepository<Users>
    {
        //void Add(Users users);
        //IEnumerable<Users> GetAll();
        //void Edit(Users users);
        //void Delete(Users users);
        //void Save();
        Users Find(int? id);

    }
    public class UserRepository : GenericRepository<TicketsEntities, Users>, IUserRepository
    {
        // accessing data 
        private TicketsEntities db = new TicketsEntities();
        //public void Add(Users users)
        //{
        //    db.Users.Add(users);
        //}

        //public void Delete(Users users)
        //{
        //    db.Users.Remove(users);
        //}

        //public void Edit(Users users)
        //{
        //    db.Entry(users).State = EntityState.Modified;
        //}

        public Users Find(int? id)
        {
            return db.Users.Find(id);
        }

        //public IEnumerable<Users> GetAll()
        //{
        //    return db.Users.Include(u => u.UserStatus);
        //}

        //public void Save()
        //{
        //    db.SaveChanges();
        //}

        public override void Delete(Users user)
        {

            // implementing soft delete
            user.IsDeleted = true;
            base.Edit(user);
        }
    }
}
