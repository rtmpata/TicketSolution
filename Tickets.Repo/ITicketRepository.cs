﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;
using System.Data.Entity;

namespace Tickets.Repo
{
    public interface ITicketRepository : IGenericRepository<Tickets.Data.Tickets>
    {
        //void Add(Tickets.Data.Tickets tickets);
        //IEnumerable<Tickets.Data.Tickets> GetAll();
        //void Edit(Tickets.Data.Tickets tickets);
        //void Delete(Tickets.Data.Tickets tickets);
        //void Save();
        Tickets.Data.Tickets Find(int? id);
    }

    public class TicketRepository : GenericRepository<TicketsEntities, Tickets.Data.Tickets>, ITicketRepository
    {
        private TicketsEntities db = new TicketsEntities();
        //public void Add(Data.Tickets tickets)
        //{
        //    db.Tickets.Add(tickets);
        //}

        //public void Delete(Data.Tickets tickets)
        //{
        //    db.Tickets.Remove(tickets);
        //}

        //public void Edit(Data.Tickets tickets)
        //{
        //    db.Entry(tickets).State = EntityState.Modified;
        //}

        public Data.Tickets Find(int? id)
        {
            return db.Tickets.Find(id);
        }

        //public IEnumerable<Data.Tickets> GetAll()
        //{
        //    return db.Tickets.Include(t => t.TicketCategory).Include(t => t.TicketStatus).Include(t => t.Users).Include(t => t.Users1);
        //}

        //public void Save()
        //{
        //    db.SaveChanges();
        //}

        public override void Delete(Data.Tickets model)
        {
            //base.Delete(model);

            // soft delete
            model.IsDeleted = true;
            base.Edit(model);
        }
    }
}
