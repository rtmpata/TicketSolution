﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;

namespace Tickets.Repo
{
    public interface IGenericRepository<T>
    {
        void Add(T model);
        IQueryable<T> GetAll();
        T GetSingle(int id );
        void Edit(T model);
        void Delete(T model);
        void Save();
    }

    // when we do need different DbContext we can pass the context name as
    public abstract class GenericRepository<C,T> : IGenericRepository<T> where T : class where C : DbContext, new()
    {

        //private TicketsEntities db = new TicketsEntities();
        // let's take DbContext from outside

        //public GenericRepository()
        //{
        //    db = new C();
        //}

        private C db = new C();

        private C context;

        public C Context
        {
            get { return db; }
            set { db = value; }
        }

        public virtual void Add(T model)
        {
            db.Set<T>().Add(model);
        }

        public virtual void Delete(T model)
        {
            db.Set<T>().Remove(model);
        }

        public virtual void Edit(T model)
        {
            db.Entry(model).State = EntityState.Modified;
        }

        //public virtual IEnumerable<T> GetAll();
        public virtual IQueryable<T> GetAll()
        {
            return db.Set<T>();
        }

        public virtual void Save()
        {
            db.SaveChanges();
        }

        public T GetSingle(int id )
        {
           return db.Set<T>().Find(id);
        }
    }

    #region without multible context
        // public abstract class GenericRepository<T> : IGenericRepository<T> where T :class
        //{
        //    private TicketsEntities db = new TicketsEntities();

        //    public virtual void Add(T model)
        //    {
        //        db.Set<T>().Add(model);
        //    }

        //    public virtual void Delete(T model)
        //    {
        //        db.Set<T>().Remove(model);
        //    }

        //    public virtual void Edit(T model)
        //    {
        //        db.Entry(model).State = EntityState.Modified;
        //    }

        //    public virtual IEnumerable<T> GetAll()
        //    {
        //        return db.Set<T>();
        //    }

        //    public virtual void Save()
        //    {
        //        db.SaveChanges();
        //    }
        //}

        #endregion
}
