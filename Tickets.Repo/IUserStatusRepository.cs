﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;

namespace Tickets.Repo
{
    public interface IUserStatusRepository : IGenericRepository<UserStatus>
    {
        //void Add(UserStatus userStatus);
        //IEnumerable<UserStatus> Getall();
        //void Edit(UserStatus userStatus);
        //void Delete(UserStatus userStatus);
        UserStatus Find(int? id);
        //void Save();

    }

    public class UserStatusRepository : GenericRepository<TicketsEntities, UserStatus>, IUserStatusRepository
    {
        TicketsEntities db = new TicketsEntities();
        //public void Add(UserStatus userStatus)
        //{
        //    db.UserStatus.Add(userStatus);
        //}

        //public void Delete(UserStatus userStatus)
        //{
        //    db.UserStatus.Remove(userStatus);
        //}

        //public void Edit(UserStatus userStatus)
        //{
        //    db.Entry(userStatus).State = EntityState.Modified;
        //}

        public UserStatus Find(int? id)
        {
            return db.UserStatus.Find(id);
        }

        //public IEnumerable<UserStatus> Getall()
        //{
        //    return db.UserStatus;
        //}

        //public void Save()
        //{
        //    db.SaveChanges();
        //}

        public override void Delete(UserStatus model)
        {
            //base.Delete(model);

            model.IsDeleted = true;
            base.Edit(model);
        }
    }
}
