﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.Repo
{
    public interface ITicketCategoriesRepository
    {
        void Add(TicketCategory ticketCategory);
        IEnumerable<TicketCategory> GetAll();
        void Edit(TicketCategory ticketCategory);
        void Save();
        void Delete(TicketCategory ticketCategory);
        TicketCategory Find(int? id);
        //IEnumerable<TicketCategory> SelectListWithParent(TicketCategory ticketCategory);

    }

    public class TicketCategoriesRepository : ITicketCategoriesRepository
    {
        private TicketsEntities db = new TicketsEntities();
        public void Add(TicketCategory ticketCategory)
        {
            db.TicketCategory.Add(ticketCategory);
        }

        public void Edit(TicketCategory ticketCategory)
        {
            db.Entry(ticketCategory).State = EntityState.Modified;
        }

        public TicketCategory Find(int? id)
        {
            return db.TicketCategory.Find(id);
        }

        public IEnumerable<TicketCategory> GetAll()
        {
           return db.TicketCategory.Include(t => t.TicketCategory2);
        }

        public void Delete(TicketCategory ticketCategory)
        {
            db.TicketCategory.Remove(ticketCategory);
        }

        public void Save()
        {
            db.SaveChanges();
        }

        //public IEnumerable<TicketCategory> SelectListWithParent(TicketCategory ticketCategory)
        //{
        //    return new SelectList(db.TicketCategory, "Id", "CategoryName", ticketCategory.ParentCategoryId);
        //}
    }
}
