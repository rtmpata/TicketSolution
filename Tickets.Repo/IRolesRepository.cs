﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;

namespace Tickets.Repo
{
    public interface IRolesRepository : IGenericRepository<Roles>
    {
        // CRUD
        // these are the contract
        // inplementation is carried out in different class
        //void Add(Roles role);
        //IEnumerable<Roles> GetAll();
        //void Edit(Roles role);
        //void Delete(Roles role);
        //Roles Find(int? id);

        //void Save();
    }


    // IRoleRepository is implemented here
    // GenericRepository<> is passed with DbContext and Class
    public class RolesRepository : GenericRepository<TicketsEntities, Roles>, IRolesRepository
    {
        // you are doing / implementing interface here
        // we need DbContext here

        //private TicketsEntities db = new TicketsEntities();

        #region generic repository
        //public void Add(Roles role)
        //{
        //    db.Roles.Add(role);
        //    //db.SaveChanges(); 
        //    // Let's make different method to save the change with Unit of work
        //}

        //public void Delete(Roles role)
        //{
        //    db.Roles.Remove(role);
        //}

        //public IEnumerable<Roles> GetAll()
        //{
        //    return db.Roles;
        //}

        public override IQueryable<Roles> GetAll()
        {
            // if you need to write specific feature to find oud in the roles
            // this.Context.Roles.
            return base.GetAll().Where(x => x.IsDeleted == false);
        }

        //public void Edit(Roles role)
        //{
        //    //if (ModelState.IsValid)
        //    //{
        //        db.Entry(role).State = EntityState.Modified;
        //        //db.SaveChanges(); // let's make a method Save(): Unit of work
        //    //}
        //}

        //public void Save()
        //{
        //    db.SaveChanges();
        //}
        #endregion
        //public Roles Find(int? id)
        //{
        //    return db.Roles.Find(id);
        //}

        // since there are methods in GenericRepository with virtual
        // we can override the methods here
        // this section will be excuted instead Delete section of GenericRepository
        // when soft delete is preffered


        // lets try soft delete
        public override void Delete(Roles model)
        {
            //base.Delete(model);
            //model.RoleName = ""; // instead of removing
            //model.IsDelete=true;

            // soft delete
            model.IsDeleted = true;
            base.Edit(model);
            
        }
    }
}
