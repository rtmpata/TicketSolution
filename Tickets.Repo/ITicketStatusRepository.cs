﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tickets.Data;

namespace Tickets.Repo
{
    public interface ITicketStatusRepository : IGenericRepository<TicketStatus>
    {
        //void Add(TicketStatus ticketStatus);
        //IEnumerable<TicketStatus> GetAll();
        //void Edit(TicketStatus ticketStatus);
        //void Save();
        //void Delete(TicketStatus ticketStatus);
        TicketStatus Find(int? id);
    }

    public class TicketStatusRepository : GenericRepository<TicketsEntities, TicketStatus>, ITicketStatusRepository
    {
        private TicketsEntities db = new TicketsEntities();

        //public void Add(TicketStatus ticketStatus)
        //{
        //    db.TicketStatus.Add(ticketStatus);
        //}

        //public void Edit(TicketStatus ticketStatus)
        //{
        //    db.Entry(ticketStatus).State = EntityState.Modified;
        //}

        public TicketStatus Find(int? id)
        {
            return db.TicketStatus.Find(id);
        }

        //public IEnumerable<TicketStatus> GetAll()
        //{
        //    return db.TicketStatus;
        //}

        //public void Delete(TicketStatus ticketStatus)
        //{
        //    db.TicketStatus.Remove(ticketStatus);
        //}

        //public void Save()
        //{
        //    db.SaveChanges();
        //}

        public override void Delete(TicketStatus model)
        {
            //base.Delete(model);
            model.IsDeleted = true;
            base.Edit(model);
        }
    }
}
