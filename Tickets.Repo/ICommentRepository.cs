﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Tickets.Data;

namespace Tickets.Repo
{
    public interface ICommentRepository
    {
        void Add(Comments comments);
        IEnumerable<Comments> GetAll();
        void Edit(Comments comments);
        void Save();
        void Delete(Comments comments);
        Comments Find(int? id);
    }

    public class CommentRepository : ICommentRepository
    {
        private TicketsEntities db = new TicketsEntities();
        public void Add(Comments comments)
        {
            db.Comments.Add(comments);
        }

        public void Delete(Comments comments)
        {
            db.Comments.Remove(comments);
        }

        public void Edit(Comments comments)
        {
            db.Entry(comments).State = EntityState.Modified;
        }

        public Comments Find(int? id)
        {
            return db.Comments.Find(id);
        }

        public IEnumerable<Comments> GetAll()
        {
            return db.Comments.Include(c => c.Tickets).Include(c => c.Users);
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
