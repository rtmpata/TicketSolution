﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tickets.UI.Startup))]
namespace Tickets.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
