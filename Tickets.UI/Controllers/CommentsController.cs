﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class CommentsController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        // base
        private ICommentRepository _repoCom;

        // Constructor
        public CommentsController()
        {
            _repoCom = new CommentRepository(); // child class
            // base will exposes the contract 
            // where child class will complete the contract
        }

        // GET: Comments
        public ActionResult Index()
        {
            //var comments = db.Comments.Include(c => c.Tickets).Include(c => c.Users);
            var comments = _repoCom.GetAll();
            return View(comments.ToList());
        }

        // GET: Comments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Comments comments = db.Comments.Find(id);
            Comments comments = _repoCom.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            return View(comments);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title");
            ViewBag.CommentBy = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Comments1,TicketId,CommentBy,CommentDate")] Comments comments)
        {
            if (ModelState.IsValid)
            {
                //db.Comments.Add(comments);
                //db.SaveChanges();

                _repoCom.Add(comments);
                _repoCom.Save();
                return RedirectToAction("Index");
            }

            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comments.TicketId);
            ViewBag.CommentBy = new SelectList(db.Users, "Id", "UserName", comments.CommentBy);
            return View(comments);
        }

        // GET: Comments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Comments comments = db.Comments.Find(id);
            Comments comments = _repoCom.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comments.TicketId);
            ViewBag.CommentBy = new SelectList(db.Users, "Id", "UserName", comments.CommentBy);
            return View(comments);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Comments1,TicketId,CommentBy,CommentDate")] Comments comments)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(comments).State = EntityState.Modified;
                //db.SaveChanges();

                _repoCom.Edit(comments);
                _repoCom.Save();
                return RedirectToAction("Index");
            }
            ViewBag.TicketId = new SelectList(db.Tickets, "Id", "Title", comments.TicketId);
            ViewBag.CommentBy = new SelectList(db.Users, "Id", "UserName", comments.CommentBy);
            return View(comments);
        }

        // GET: Comments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Comments comments = db.Comments.Find(id);

            Comments comments = _repoCom.Find(id);
            if (comments == null)
            {
                return HttpNotFound();
            }
            return View(comments);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Comments comments = db.Comments.Find(id);
            //db.Comments.Remove(comments);
            //db.SaveChanges();

            Comments comments = _repoCom.Find(id);
            _repoCom.Delete(comments);
            _repoCom.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
