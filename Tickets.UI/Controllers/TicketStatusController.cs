﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class TicketStatusController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        // base
        private ITicketStatusRepository _repoTS;

        // Constructor
        public TicketStatusController()
        {
            _repoTS = new TicketStatusRepository(); // child class
            // base will be interfacing component
            // child class will implement method

        }

        // GET: TicketStatus
        public ActionResult Index()
        {
            // only TicketStatus with IsDeleted flag set False
            // are shown
            var ticketsStatus = _repoTS.GetAll().Where(x=>x.IsDeleted == false);

            return View(ticketsStatus.ToList());
        }

        // GET: TicketStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketStatus ticketStatus = db.TicketStatus.Find(id);
            TicketStatus ticketStatus = _repoTS.Find(id);
            if (ticketStatus == null)
            {
                return HttpNotFound();
            }
            return View(ticketStatus);
        }

        // GET: TicketStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TicketStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StatusName")] TicketStatus ticketStatus)
        {
            if (ModelState.IsValid)
            {
                //db.TicketStatus.Add(ticketStatus);
                //db.SaveChanges();

                _repoTS.Add(ticketStatus);
                _repoTS.Save();
                return RedirectToAction("Index");
            }

            return View(ticketStatus);
        }

        // GET: TicketStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketStatus ticketStatus = db.TicketStatus.Find(id);

            TicketStatus ticketStatus = _repoTS.Find(id);
            if (ticketStatus == null)
            {
                return HttpNotFound();
            }
            return View(ticketStatus);
        }

        // POST: TicketStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StatusName")] TicketStatus ticketStatus)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(ticketStatus).State = EntityState.Modified;
                //db.SaveChanges();
                _repoTS.Edit(ticketStatus);
                _repoTS.Save();
                return RedirectToAction("Index");
            }
            return View(ticketStatus);
        }

        // GET: TicketStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketStatus ticketStatus = db.TicketStatus.Find(id);

            TicketStatus ticketStatus = _repoTS.Find(id);
            if (ticketStatus == null)
            {
                return HttpNotFound();
            }
            return View(ticketStatus);
        }

        // POST: TicketStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //TicketStatus ticketStatus = db.TicketStatus.Find(id);
            //db.TicketStatus.Remove(ticketStatus);
            //db.SaveChanges();


            //TicketStatus ticketStatus =  _repoTS.Find(id);
            //_repoTS.Delete(ticketStatus);
            //_repoTS.Save();

            TicketStatus ticketStatus = _repoTS.GetAll().FirstOrDefault(x => x.Id == id);
            _repoTS.Delete(ticketStatus);
            _repoTS.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
