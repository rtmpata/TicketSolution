﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class RolesController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        // base
        private IRolesRepository _repRoles;
        // constructor
        public RolesController()
        {
            _repRoles = new RolesRepository(); // child and concrete class
                                               // IRolesRepository is in Tickets.Repo namespace
                                               // IRolesRepository is base and RolesRepository is child class
                                               // when you have repos, extension will be from interface but 
                                               // concrete class is also instantiated here
                                               // so implementation will come from child/concrete class RolesRepository

        }


        // GET: Roles
        public ActionResult Index()
        {
            var roles = _repRoles.GetAll().Where(x => x.IsDeleted == false);
            return View(roles.ToList());
        }

        // GET: Roles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            //Roles roles = _repRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // GET: Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RoleName")] Roles roles)
        {
            if (ModelState.IsValid)
            {
                //db.Roles.Add(roles);
                //db.SaveChanges();
                // using repository
                _repRoles.Add(roles); // from IRolesRepository
                _repRoles.Save();   // SaveChanges() from IRolesRepository
                return RedirectToAction("Index");
            }

            return View(roles);
        }

        // GET: Roles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            //Roles roles = _repRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RoleName")] Roles roles)
        {
            if (ModelState.IsValid)
            {
                _repRoles.Edit(roles);
                _repRoles.Save();
                //db.Entry(roles).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roles);
        }

        // GET: Roles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles roles = db.Roles.Find(id);
            //Roles roles = _repRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Roles roles = _repRoles.GetAll().FirstOrDefault(x => x.Id == id);
            if (roles != null)
            {
                //Roles roles = _repRoles.Find(id);
                //db.Roles.Remove(roles); 
                _repRoles.Delete(roles);
                //db.SaveChanges();
                _repRoles.Save();
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
