﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class UserStatusController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        // Base
        private IUserStatusRepository _repoUS;

        // constructor
        public UserStatusController()
        {
            _repoUS = new UserStatusRepository();  // child class
            // IUserStatusRepository is base and UserStatusRepository is child class
            // base class has a contract where
            // child class has implementations of the contract
        }


        // GET: UserStatus
        public ActionResult Index()
        {
            var userStatus = _repoUS.GetAll().Where(x=>x.IsDeleted == false);
            return View(userStatus.ToList());
        }

        // GET: UserStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UserStatus userStatus = db.UserStatus.Find(id);
            UserStatus userStatus = _repoUS.Find(id);
            if (userStatus == null)
            {
                return HttpNotFound();
            }
            return View(userStatus);
        }

        // GET: UserStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserStatus1")] UserStatus userStatus)
        {
            if (ModelState.IsValid)
            {
                //db.UserStatus.Add(userStatus);
                //db.SaveChanges();

                _repoUS.Add(userStatus);
                _repoUS.Save();

                return RedirectToAction("Index");
            }

            return View(userStatus);
        }

        // GET: UserStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UserStatus userStatus = db.UserStatus.Find(id);
            UserStatus userStatus = _repoUS.Find(id);
            if (userStatus == null)
            {
                return HttpNotFound();
            }
            return View(userStatus);
        }

        // POST: UserStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserStatus1")] UserStatus userStatus)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(userStatus).State = EntityState.Modified;
                //db.SaveChanges();

                _repoUS.Add(userStatus);
                _repoUS.Save();
                return RedirectToAction("Index");
            }
            return View(userStatus);
        }

        // GET: UserStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //UserStatus userStatus = db.UserStatus.Find(id);
            UserStatus userStatus = _repoUS.Find(id);
            if (userStatus == null)
            {
                return HttpNotFound();
            }
            return View(userStatus);
        }

        // POST: UserStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //UserStatus userStatus = db.UserStatus.Find(id);
            //db.UserStatus.Remove(userStatus);
            //db.SaveChanges();

            //UserStatus userStatus = _repoUS.Find(id);
            //_repoUS.Delete(userStatus);
            //_repoUS.Save();

            // using soft delete
            UserStatus userStatus = _repoUS.GetAll().FirstOrDefault(x => x.Id == id);
            if (userStatus != null)
            {
                _repoUS.Delete(userStatus);
                _repoUS.Save();
            }
            

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
