﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class TicketCategoriesController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        //base
        private ITicketCategoriesRepository _repoTCat;

        // Constructor
        public TicketCategoriesController()
        {
            _repoTCat = new TicketCategoriesRepository(); // child class
            // TicketCategoriesRepository is child class of ITicketCategoriesRepository
            // methods are shown by interface where
            // child class will implement the method
        }

        // GET: TicketCategories
        public ActionResult Index()
        {
            //var ticketCategory = db.TicketCategory.Include(t => t.TicketCategory2);
            return View(_repoTCat.GetAll().ToList());
        }

        // GET: TicketCategories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketCategory ticketCategory = db.TicketCategory.Find(id);

            TicketCategory ticketCategory = _repoTCat.Find(id);
            if (ticketCategory == null)
            {
                return HttpNotFound();
            }
            return View(ticketCategory);
        }

        // GET: TicketCategories/Create
        public ActionResult Create()
        {
            // what if we could make this SelectList in repository
            ViewBag.ParentCategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName");

            return View();
        }

        // POST: TicketCategories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CategoryName,ParentCategoryId")] TicketCategory ticketCategory)
        {
            if (ModelState.IsValid)
            {
                //db.TicketCategory.Add(ticketCategory);
                //db.SaveChanges();

                _repoTCat.Add(ticketCategory); // adding through the repository
                _repoTCat.Save();
                return RedirectToAction("Index");
            }

            ViewBag.ParentCategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", ticketCategory.ParentCategoryId);
            return View(ticketCategory);
        }

        // GET: TicketCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketCategory ticketCategory = db.TicketCategory.Find(id);
            TicketCategory ticketCategory = _repoTCat.Find(id);
            if (ticketCategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentCategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", ticketCategory.ParentCategoryId);
            return View(ticketCategory);
        }

        // POST: TicketCategories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CategoryName,ParentCategoryId")] TicketCategory ticketCategory)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(ticketCategory).State = EntityState.Modified;
                //db.SaveChanges();

                _repoTCat.Edit(ticketCategory);
                _repoTCat.Save();
                return RedirectToAction("Index");
            }
            ViewBag.ParentCategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", ticketCategory.ParentCategoryId);
            return View(ticketCategory);
        }

        // GET: TicketCategories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TicketCategory ticketCategory = db.TicketCategory.Find(id);
            TicketCategory ticketCategory = _repoTCat.Find(id);
            if (ticketCategory == null)
            {
                return HttpNotFound();
            }
            return View(ticketCategory);
        }

        // POST: TicketCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //TicketCategory ticketCategory = db.TicketCategory.Find(id);
            TicketCategory ticketCategory = _repoTCat.Find(id);
            //db.TicketCategory.Remove(ticketCategory);
            _repoTCat.Delete(ticketCategory);
            //db.SaveChanges();
            _repoTCat.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
