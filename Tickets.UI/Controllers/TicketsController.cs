﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Data;
using Tickets.Repo;

namespace Tickets.UI.Controllers
{
    public class TicketsController : Controller
    {
        private TicketsEntities db = new TicketsEntities();

        // base
        private ITicketRepository _repoTicket;

        // Constructor
        public TicketsController()
        {
            _repoTicket = new TicketRepository(); // child
        }

        // GET: Tickets
        public ActionResult Index()
        {
            //var tickets = db.Tickets.Include(t => t.TicketCategory).Include(t => t.TicketStatus).Include(t => t.Users).Include(t => t.Users1);

            var tickets = _repoTicket.GetAll().Where(x=>x.IsDeleted == false);
            return View(tickets.ToList());
        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Tickets.Data.Tickets tickets = db.Tickets.Find(id);
            Tickets.Data.Tickets tickets = _repoTicket.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // GET: Tickets/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName");
            ViewBag.StatusId = new SelectList(db.TicketStatus, "Id", "StatusName");
            ViewBag.AssignedBy = new SelectList(db.Users, "Id", "UserName");
            ViewBag.AssignedTo = new SelectList(db.Users, "Id", "UserName");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,AssignedBy,CreatedDate,AssignedTo,StatusId,CategoryId")] Tickets.Data.Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                //db.Tickets.Add(tickets);
                //db.SaveChanges();

                _repoTicket.Add(tickets);
                _repoTicket.Save();

                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", tickets.CategoryId);
            ViewBag.StatusId = new SelectList(db.TicketStatus, "Id", "StatusName", tickets.StatusId);
            ViewBag.AssignedBy = new SelectList(db.Users, "Id", "UserName", tickets.AssignedBy);
            ViewBag.AssignedTo = new SelectList(db.Users, "Id", "UserName", tickets.AssignedTo);
            return View(tickets);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Tickets.Data.Tickets tickets = db.Tickets.Find(id);
            Tickets.Data.Tickets tickets = _repoTicket.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", tickets.CategoryId);
            ViewBag.StatusId = new SelectList(db.TicketStatus, "Id", "StatusName", tickets.StatusId);
            ViewBag.AssignedBy = new SelectList(db.Users, "Id", "UserName", tickets.AssignedBy);
            ViewBag.AssignedTo = new SelectList(db.Users, "Id", "UserName", tickets.AssignedTo);
            return View(tickets);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,AssignedBy,CreatedDate,AssignedTo,StatusId,CategoryId")] Tickets.Data.Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(tickets).State = EntityState.Modified;
                //db.SaveChanges();

                _repoTicket.Edit(tickets);
                _repoTicket.Save();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.TicketCategory, "Id", "CategoryName", tickets.CategoryId);
            ViewBag.StatusId = new SelectList(db.TicketStatus, "Id", "StatusName", tickets.StatusId);
            ViewBag.AssignedBy = new SelectList(db.Users, "Id", "UserName", tickets.AssignedBy);
            ViewBag.AssignedTo = new SelectList(db.Users, "Id", "UserName", tickets.AssignedTo);
            return View(tickets);
        }

        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Tickets.Data.Tickets tickets = db.Tickets.Find(id);
            Tickets.Data.Tickets tickets = _repoTicket.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Tickets.Data.Tickets tickets = db.Tickets.Find(id);
            //db.Tickets.Remove(tickets);
            //db.SaveChanges();

            //Tickets.Data.Tickets tickets = _repoTicket.Find(id);
            //_repoTicket.Delete(tickets);
            //_repoTicket.Save();

            // Soft Delete
            var tickets = _repoTicket.GetAll().FirstOrDefault(X => X.Id == id);
            if (tickets != null)
            {
                _repoTicket.Delete(tickets);
                _repoTicket.Save();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
